const express = require('express');
const router = express.Router();

const { getNotas, 
        addNotas,
        updateNotas,
        deleteNotas

    } = require('../controllers/notascontro')


router.route('/')
    .get(getNotas)
    .post(addNotas);

router.route('/:id') //esto es lo mismo que http://localhost:3000/notas/id ....
    .put(updateNotas)
    .delete(deleteNotas);


module.exports = router;