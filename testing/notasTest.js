const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../database.json');
const { response } = require('express');

chai.should();

chai.use(chaiHttp);

describe('Notas de la API', () =>{

    describe("GET/api/notas", () => {
        it('It should GET all the notes', (done) => {
            chai.request(server)

            .get("/api/notas")
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('array');
                response.body.lenght.should.be.eq(3);
            done();
            })
        })

    })
   
})