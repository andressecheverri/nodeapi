const { notas } = require('../database.json');

module.exports = {
    
    getNotas: (req, res) => {
        res.json({notas})
    },

    addNotas: (req, res) => {
        const { nombre } = req.body;
        notas.push({
            
            id: notas.length,  // hará que los id se vayan poniendo en orden
            nombre      //mediante postman se irán agregando los datos
        });
        res.json({
            'success': true,
            'msg': 'añadido correctamente'  // ponemos un mensaje para que nos avise de que el dato se ha añadido correctamente
        });
    },

    updateNotas: (req, res) => {
        const { id } = req.params;  //con esto solo obtendriamos el id
        const { nombre } = req.body;

        notas.forEach((notas, i) => {
            if(notas.id === Number(id)){
                notas.nombre = nombre;
            }
        });  // pasa una nota y luego el índice de recorrido, empezará en 0,1,2 ...
        res.json({
            'success': true,
            'msg': 'actualizado correctamente'  // ponemos un mensaje para que nos avise de que el dato se ha añadido correctamente
        }); 
    },

    deleteNotas: (req, res) => {
        const { id } = req.params;
        notas.forEach((notas, i) => {
            if(notas.id === Number(id)){
                notas.splice(i, 1);
            }
        
        });
        res.json({
            'success': true,
            'msg': 'borrado correctamente'  // ponemos un mensaje para que nos avise de que el dato se ha borrado correctamente
        }); 

    }
}