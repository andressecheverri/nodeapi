const express = require('express');
const app = express();

const morgan = require('morgan');
const bodyParser = require('body-parser');

const notasRoutes = require('./routes/notas')

//settings
app.set('json spaces', 2);// agrega un espacio en el localhost

//middlewares
app.use(morgan('dev'));//trabajamos en develop
app.use(bodyParser.json());//convierte los archivos en un json
app.use(bodyParser.urlencoded({extended: false}));


//routes
app.use('/notas', notasRoutes);
//static files

//starting the server
app.listen(3000, () =>{
    console.log('listen on port', 3000);
});